import { Component } from '@angular/core';
import { HttpClient } from "@angular/common/http";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'adresse-angular';
  data:any = [];
  address: any = String;
  longitude:any = Number;
  latitude:any = Number;


 constructor(private http: HttpClient) {}

 // récuperation des coordonnées sur l'api
 getAddress(address: any){
  this.address = address.replace(/[^a-zA-Z0-9]/g, "+");
  const url ='https://api-adresse.data.gouv.fr/search/?q='+ this.address;
  this.http.get(url).subscribe((res)=>{
    this.data = res;
  })
 }

 // Envoi des coordonnées sur la vue
 sendCoord(long: number, lat: number){
    console.log("long :" + long + " lat : " + lat);
    this.longitude = long;
    this.latitude = lat;
 }
}
