import { Component, AfterViewInit, Input, SimpleChanges } from '@angular/core';
import * as L from 'leaflet';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.sass']
})
export class MapComponent implements AfterViewInit {
  private map: any;
  // Récuperation des coordonnées du parentComponent
  @Input() longitude:any= Number;
  @Input() latitude:any= Number;


// Initialisatio ndu map
  private initMap(): void {
    this.map = L.map('map', {
      center: [ 43.921098, 2.138793],
      zoom: 3
    });

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 3,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);
  }

  constructor() { }

  ngAfterViewInit(): void {
    this.initMap();
  }

  // Mise en place du marker sur le plan
  placeMarker() {
    const location = [this.latitude, this.longitude];
    this.map.panTo(location);
    const marker = L.marker([this.latitude, this.longitude]);
    marker.addTo(this.map);
  }

  // Evenement pour mettre le marker à chaque changement de longitude ou latitude
  ngOnChanges(changes: SimpleChanges) {
    this.placeMarker();
  }
}
